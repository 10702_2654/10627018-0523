package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public Button btnwk;
    public ImageView iv;
    public ProgressBar pbwk;

    public void onWk(ActionEvent actionEvent) {
        List<String> imgepath=new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            imgepath.add("/resources/".concat(String.valueOf(i).concat(".jpg")));
        }
        Timeline timeline=new Timeline();
        timeline.setCycleCount(timeline.INDEFINITE);
        timeline.setAutoReverse(false);
        for(int i=1;i<4;i++) {
            int j=i;
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(i+1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            iv.setImage(new Image(getClass().getResource(imgepath.get(j)).toString()));

                        }
                    })

            );
        }
        timeline.play();
        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                final int max = 100;
                pbwk.setVisible(true);
                for (int i = 1; i < max; i++) {
                    Thread.sleep(500);
                    updateProgress(i, max);
                }
                return null;
            }
            @Override
            protected void scheduled(){
                super.scheduled();
                pbwk.setVisible(true);
                timeline.play();
                iv.setVisible(true);
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                pbwk.setVisible(false);
                timeline.stop();
                iv.setVisible(false);

            }

            @Override
            protected void failed() {
                super.failed();
            }
        };
        pbwk.progressProperty().bind(task.progressProperty());
        pbwk.setVisible(true);
        new Thread(task).start();



    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pbwk.setVisible(false);


    }
}